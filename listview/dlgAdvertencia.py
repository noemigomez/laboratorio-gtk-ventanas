#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgAdvertencia():

    def __init__(self, titulo=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")

        self.adv = self.builder.get_object("dlgAdvertencia")
        self.adv.resize(200, 300)
        self.adv.set_property("text",
                              "Debe rellenar ambas secciones.")
        self.adv.set_property("secondary_text", "*Nombre\n*Apellido")

        # boton aceptar
        boton_ok = self.builder.get_object("aceptar")
        boton_ok.connect("clicked", self.btn_ok)

        self.adv.show_all()

    def btn_ok(self, btn=""):
        self.adv.destroy()
