#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgEliminar():

    def __init__(self, titulo="Advertencia"):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")

        self.eliminar = self.builder.get_object("dlgEliminar")
        self.eliminar.set_title(titulo)
        self.eliminar.resize(200, 300)

        self.btn_aceptar = self.builder.get_object("button1")
        self.btn_aceptar.connect("clicked", self.borrar)

        self.btn_cancel = self.builder.get_object("button2")
        self.btn_cancel.connect("clicked", self.no_borrar)

    def borrar(self, btn=0):
        self.eliminar.destroy()

    def no_borrar(self, btn=0):
        self.eliminar.destroy()
